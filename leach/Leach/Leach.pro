#-------------------------------------------------
#
# Project created by QtCreator 2016-06-05T08:44:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Leach
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    node.cpp

HEADERS  += mainwindow.h \
    node.h

FORMS    += mainwindow.ui
