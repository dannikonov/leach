#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <node.h>
#include <QGraphicsScene>
#include <QTimer>
#include <cmath>
#include <QTextEdit>

// Все массивы узлов будем называть типом nodes
typedef QVector< Node *> nodes;

namespace Ui {
class MainWindow;
}

// Класс главного окна
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    // Слот для сигнала клик по кнопке старт
    void on_pb_start_clicked();
    // Слот для сигнала клик по кнопке следующий раунд
    void on_pb_next_clicked();
    // Слот для сигнала клик по любой радиокнопке выбора режима (статика/бпла)
    void on_radiobutton_changed();

private:
    Ui::MainWindow *_ui;
    /* field */
    double _w, _h, _z; // ширина, высота поля и высота бпла
    int _n_node, _n_head, _round; // число узлов, число головных узлов, счетчик раундов
    bool _dron_mode; // Режимы. Статика - 0, бпла - 1

    /* nodes */
    double _e0, _ee, _ea, _p, _k; // всякик константы

    /* coords */
    Node *_gw; // шлюз/бпла
    nodes _nodes, _heads; // массивы узлов и головных узлов
    QMap<Node *, nodes> _clusters; // кластеры. Ключ - головной узел кластера, значение - ноды, входящие в кластер

    // это нужно для работы рисовалки
    QGraphicsScene *_scene;
    double _node_size,_scene_x_koef, _scene_y_koef;


    // компаратор для сортировки вершин в алгоритме leach
    int (*_sortfunc)(const QPair<int, double> &a, const QPair<int, double> &b);

    void generate_nodes(); // создать узлы
    void next_round(); // следующий раунд

    // функции рисовалки
    void draw_graph() const; // нарисовать все вершины
    void draw_node(Node *node) const; // нарисовать 1 вершину в зависимости от типа
    void draw_gw(Node *node) const; // нарисовать шлюз
    void draw_ellipse(Node *node, Qt::GlobalColor pen, Qt::GlobalColor brush) const;
    void draw_square(Node *node, Qt::GlobalColor pen, Qt::GlobalColor brush) const;
    void draw_triangle(Node *node, Qt::GlobalColor pen, Qt::GlobalColor brush) const;
    void update_scene(); // обновление раунда и рисовалки

    void leach(); // инициализация рандомных чисел в алгоритме leach
    // компаратор для сортировки вершин в алгоритме leach
    static int leach_comparator(const QPair<int, double> &a, const QPair<int, double> &b);
    // выбор 5 головных узлов и заполнение массива головных узлов.
    void set_head_nodes( QVector< QPair<int, double> > &t);
    double r(Node *node, Node *head_node) const; // вычисление расстояния между двумя узлами
    // кластеризация узлов по расстояние до головного
    void set_clusters();
    // передача данных от головных узлов шлюзу (считаем и выводим на экран энергию)
    void operate();
};


#endif // MAINWINDOW_H
