#include "node.h"

Node::Node(double x, double y, double z, double e, int n):
_x(x), _y(y), _z(z), _e(e), _n(n), _type(T_NODE), _dead(false)
{
//    qDebug() << _n << " - node with x: " << _x << " y: " << _y << " e: " << _e << " constructed";
}

Node::~Node() {
//    qDebug() << "destruct node";
}

int Node::n() const { return _n; }

double Node::x() const { return _x; }
double Node::y() const { return _y; }
double Node::z() const { return _z; }
double Node::e() const { return _e; }
node_type Node::type() const { return _type; }
bool Node::dead() const { return _dead; }

void Node::x(double x) { _x = x; }
void Node::y(double y) { _y = y; }
void Node::z(double z) { _z = z; }
void Node::e(double e) { _e = e; }
void Node::type(node_type type) { _type = type; }
void Node::dead(bool dead) { _dead = dead; }

void Node::move(Node *target) {
    _x = target->x();
    _y = target->y();
}
