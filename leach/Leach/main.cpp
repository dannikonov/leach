#include "mainwindow.h"
#include <QApplication>

// Создание главного окна
int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
