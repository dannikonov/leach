#include "mainwindow.h"
#include "ui_mainwindow.h"
/** @todo
 * LEACH
 * 1. По-другому считать головные вершины
 * 2. Бывшие головными вершины не участвуют в генерации, т.е. 0 - нормальное число как результат рандома.
 *
 * Вершины
 * 1. Добавить флаг "была головной"
 * 2. Отображать по-другому те вершины,которые были головными
 */

// конструктор
MainWindow::MainWindow(QWidget *parent) :
    // инициализация полей класса пустыми значениями
    QMainWindow(parent),
    _ui(new Ui::MainWindow),
    _w(0), _h(0), _z(0),
    _n_node(0), _n_head(0), _round(0), _dron_mode(false),
    _e0(0), _ee(0), _ea(0), _p(0), _k(0),
    _gw(0), _nodes(), _heads(), _clusters(QMap<Node *, nodes>()),
    _scene(new QGraphicsScene(parent)),
    _node_size(10), _scene_x_koef(0), _scene_y_koef(0)
{
    _ui->setupUi(this);
    // подключили слот для реакции на клик радиокнопок
    connect(_ui->rb_static_gw, SIGNAL(clicked()), this, SLOT(on_radiobutton_changed()));
    connect(_ui->rb_dron, SIGNAL(clicked()), this, SLOT(on_radiobutton_changed()));

    // создали и настроили рисовалку
    _scene = new QGraphicsScene(0,0,
                                _ui->gv_scene->width() - 20,
                                _ui->gv_scene->height() - 20,
                                _ui->gv_scene);
    _ui->gv_scene->setScene(_scene);
    _ui->gv_scene->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _ui->gv_scene->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // установили видимость дополнительного поля для режима бпла
    _ui->gb_dron->setVisible(_dron_mode);
}

// деструктор для освобождения памяти
MainWindow::~MainWindow() {
    if (_gw) delete _gw;
    for (int i = 0; i < _nodes.size(); ++i) {
        if (_nodes[i]) delete _nodes[i];
    }

    delete _ui;
}

// Слот для сигнала клик по кнопке старт
void MainWindow::on_pb_start_clicked() {
    // инициализация полей класса значениями из гуи
    _w = _ui->le_width->text().toDouble();
    _h = _ui->le_height->text().toDouble();
    _n_node = _ui->le_n_node->text().toInt();
    _n_head = _ui->le_n_head->text().toInt();

    _e0 = _ui->le_e0->text().toDouble();
    _ea = _ui->le_ea->text().toDouble() * pow(10, -12);
    _ee = _ui->le_ee->text().toDouble() * pow(10, -9);
    _p =  _ui->le_p->text().toDouble() / 100;
    _k =  _ui->le_k->text().toDouble();

    // включили кнопку "следующий раунд"
    _ui->pb_next->setEnabled(true);

    // очистка и настройка полей для вывода вершин и энергий
    _ui->te_nodes_list->clear();
    _ui->te_nodes_list->append(QString().sprintf("%3s|%9s|%9s|%9s|%9s", "N", "x", "y", "Расст", "Головной", "Активная"));

    _ui->te_energy_list->clear();
    _ui->te_energy_list->append(QString().sprintf("%3s|%6s|%6s|%9s","N", "Eсум", "Eср", "Расст. ср."));

    // сгенерили узлы
    this->generate_nodes();

    this->update_scene();
}

// Слот для сигнала клик по кнопке следующий раунд
void MainWindow::on_pb_next_clicked() {
    this->next_round();
}

// Слот для сигнала клик по любой радиокнопке выбора режима (статика/бпла)
void MainWindow::on_radiobutton_changed() {
    _dron_mode = _ui->rb_dron->isChecked();
    _ui->gb_dron->setVisible(_dron_mode);
    _ui->le_z->setText(QString::number(50));
}

// создание узлов
void MainWindow::generate_nodes() {
    _heads.clear();
    _nodes.clear();
    qDebug() << "generate nodes";
    double x, y;
    for (int i = 0; i < _n_node; ++i) {
        // генерим координаты
        x = _w * ((double)rand() / (double)(RAND_MAX));
        y = _h * ((double)rand() / (double)(RAND_MAX));

        // если бпла, то z нужна, в противном случае ее можно принять равной 0 для унификации расчетов.
        if (_dron_mode) {
            _z = _ui->le_z->text().toDouble();
        } else {
            _z = 0;
        }

        // набиваем массив с узлами
        _nodes.insert(i, new Node(x, y, 0, _e0, i));
    }

    // сгенерили шлюз.
    _gw = new Node(_w / 2, _h / 2, _z, 10000, -1);
}

// следующий раунд
void MainWindow::next_round() {
    // в начале нового раунда переводим старые головные узлы в состояние "был головным"

    bool new_cycle = true;
    nodes::iterator n_it = _nodes.begin();
    for (; n_it != _nodes.end(); ++n_it) {
        // проверяем, остались ли обычные узлы
        if ((*n_it)->type() == T_NODE) { new_cycle = false; }
        if ((*n_it)->type() == T_HEAD) {
            (*n_it)->type(T_WAS_HEAD);
        }
    }

    // если обычных узлов не осталось, обнуляем. Начинаем новый цикл сети.
    if (new_cycle) {
        for (n_it = _nodes.begin(); n_it != _nodes.end(); ++n_it) {
            (*n_it)->type(T_NODE);
        }
    }


    this->leach();
    this->set_clusters();
    this->operate();

    _round++;
    this->update_scene();
}

// рисуем все вершины из массива вершин
void MainWindow::draw_graph() const {
    _scene->clear();

    nodes::const_iterator n_it = _nodes.begin();
    for (; n_it != _nodes.end(); ++n_it) {
        this->draw_node(*n_it);
    }

    if(_dron_mode) {
        if (_heads.size()) {
            nodes::const_iterator h_it = _heads.begin();
            for (; h_it != _heads.end(); ++h_it) {
                _gw->move(*h_it);
                this->draw_gw(_gw);
            }
        }
    } else {
        this->draw_gw(_gw);
    }
}

// рисование одной вершины
void MainWindow::draw_node(Node *node) const {
    if (node->dead()) { // мертвые узлы
        this->draw_ellipse(node, Qt::black, Qt::black);
    } else {
        switch(node->type()) {
            case T_HEAD: // головной узел
                this->draw_square(node, Qt::black, Qt::red); break;
            case T_WAS_HEAD: // был головным
                this->draw_ellipse(node, Qt::black, Qt::magenta); break;
            default: // обычные живые узлы
            case T_NODE:
                this->draw_ellipse(node, Qt::black, Qt::red); break;
        }
    }
}

// рисование шлюза
void MainWindow::draw_gw(Node *node) const {
   this->draw_triangle(node, Qt::black, Qt::green);
}

void MainWindow::draw_ellipse(Node *node, Qt::GlobalColor pen, Qt::GlobalColor brush) const {    
    _scene->addEllipse(
                node->x() * _scene_x_koef  - _node_size / 2,
                node->y() * _scene_y_koef  - _node_size / 2,
                _node_size,_node_size,
                QPen(pen), QBrush(brush)
            );
}

void MainWindow::draw_square(Node *node, Qt::GlobalColor pen, Qt::GlobalColor brush) const {
    QPolygonF polygon;
    polygon << QPointF(node->x()  * _scene_x_koef - _node_size / 2, node->y() * _scene_y_koef - _node_size / 2)
            << QPointF(node->x()  * _scene_x_koef - _node_size / 2, node->y() * _scene_y_koef + _node_size / 2)
            << QPointF(node->x()  * _scene_x_koef + _node_size / 2, node->y() * _scene_y_koef + _node_size / 2)
            << QPointF(node->x()  * _scene_x_koef + _node_size / 2, node->y() * _scene_y_koef - _node_size / 2);

    _scene->addPolygon(polygon, QPen(pen), QBrush(brush));
}

void MainWindow::draw_triangle(Node *node, Qt::GlobalColor pen, Qt::GlobalColor brush) const {
    double h = _node_size * sqrt(3) / 2;
    QPolygonF polygon;
    polygon << QPointF(node->x()  * _scene_x_koef - _node_size / 2, node->y() * _scene_y_koef + h / 2)
            << QPointF(node->x()  * _scene_x_koef + _node_size / 2, node->y() * _scene_y_koef + h / 2)
            << QPointF(node->x()  * _scene_x_koef, node->y() * _scene_y_koef - h / 2);
    _scene->addPolygon(polygon, QPen(pen), QBrush(brush));
}

void MainWindow::update_scene() {
    // расчет коэффициентов для масштабирования координат (координаты узла на поле и координаты точки на рисовалке отличаются)
    _scene_x_koef = _scene->width() / _w;
    _scene_y_koef = _scene->height() / _h;

    _ui->l_round_cnt->setText(QString("Раунд: %1").arg(QString::number(_round)));

    draw_graph();
}

// "посылка" узлами рандомных чисел
void MainWindow::leach() {
    // массив пар вида "номер узла":"рандомное число"
    QVector< QPair<int, double> > t;
    nodes::iterator n_it = _nodes.begin();
    for (; n_it != _nodes.end(); ++n_it) {
        if (!(*n_it)->dead()) {
            // Если узел - обычный (не головной и не был головным)
            if ((*n_it)->type() == T_NODE) {
                t.append(QPair<int, double>((*n_it)->n(), (double)rand() / (double)(RAND_MAX)));
            }

        }
    }
    set_head_nodes(t);
}

// сравнение 2 элементов массива из функции leach()
int MainWindow::leach_comparator(const QPair<int, double> &a, const QPair<int, double> &b) {
    return a.second - b.second;
}

// выбор 5 головных узлов и заполнение массива головных узлов.
void MainWindow::set_head_nodes(QVector< QPair<int, double> > &t) {
    _sortfunc = &MainWindow::leach_comparator;
    // сортируем массив рандомных чисел от узлов (номера узлов при этом сохраняются,
    // т.е. для конкретной пары номер-число мы можем установить номер узла, сгенерившего число)
    qSort(t.begin(), t.end(), _sortfunc);

    QVector< QPair<int, double> >::const_iterator it;
    int head_nodes_need = _n_head;
    // очистили массив с головными вершинами
    _heads.clear();
    // пробегаемся по отсортированному массиву
    for (it = t.begin(); it != t.end(); ++it) {
        if(head_nodes_need) { // нашли новый головной узел
            head_nodes_need--;
            _nodes[it->first]->type(T_HEAD); // сказали вершине, что она теперь головная
            _heads.append(_nodes[it->first]); // добавили ее в массив с головными вершинами
        }
    }
}

// вычисление расстояния между 2 точками
double MainWindow::r(Node *node, Node *head_node) const {
    return sqrt(
                pow(node->x() - head_node->x(), 2)
                +
                pow(node->y() - head_node->y(), 2)
                +
                pow(node->z() - head_node->z(), 2)
            );
}

// кластеризация узлов по расстояние до головного
void MainWindow::set_clusters() {
    nodes::iterator n_it = _nodes.begin();
    nodes::iterator c_it;
    nodes::iterator h_it;

    // очищаем массив с кластерами
    _clusters.clear();

    // для каждой головной вершины инициализируем кластер
    for (h_it = _heads.begin(); h_it != _heads.end(); ++h_it) {
        QVector<Node *> cluster;
        // ключ кластера - головная вершина, массив - члены кластера
        _clusters.insert(*h_it, cluster);
    }

    // для каждого узла
    for (; n_it != _nodes.end(); ++n_it) {
        // если узел головной на данном раунде, то не делаем ничего
        if ((*n_it)->type() == T_HEAD) continue;

        // расчет минимального расстояния от текущего узла до каждого головного
        // инициализация минимального расстояния и индекса
        double range_to_min = this->r((*n_it), _heads.first());
        int number_of_min = _heads.first()->n();

        // пробегаем все головные узлы и считаем расстояние до каждого
        for (c_it = _heads.begin(); c_it != _heads.end(); ++c_it) {
            // если расстояние получилось меньше минимально найденного, менямем минимальное
            // и запоминаем индекс этого головного узла
            if(range_to_min <= this->r(*n_it, *c_it)) {
                number_of_min = (*c_it)->n();
                range_to_min = this->r(*n_it, *c_it);
            }
        }

        // фигачим эту (текущую) вершину в кластер с нужным индексом
        _clusters[_nodes[number_of_min]].append(*n_it);
    }
}

// передача данных
void MainWindow::operate() {
    qDebug() << "operate. round: " << _round;
    _ui->te_nodes_list->clear();
    _ui->te_nodes_list->append(QString().sprintf("%3s|%9s|%9s|%9s|%9s|%9s|%9s", "N", "x", "y", "Расст", "Статус", "Состояние", "E"));

    nodes::const_iterator n_it = _nodes.begin();
    double e_sum = 0, d_sum = 0;
    QString nodes_body = "";
    // Перебираем все узлы
    for (; n_it!= _nodes.end(); ++n_it) {
        double d = 0;
        QString st = "Узел";
        QString d_str = "-", e_str = "-";

        // если узел - головной, считаем энергию
        if ((*n_it)->type() == T_HEAD) {
            st = "Головной";
            // если бпла, то меняем координаты шлюза
            if(_dron_mode) {
                _gw->move(*n_it);
            }
            // считаем расстояние от головного узла до шлюза
            d = this->r(*n_it, _gw);
            d_str = QString::number(d, 'f', 6);
            // формула для расчета энергии
            double e = _ee * _k + _ea * _k * pow(d, 2);
            e_str = QString::number(e, 'f', 6);
            // суммируем энергии от всех головных узлов в раунде
            e_sum += e;
            // суммируем расстояния до всех головных узлов
            d_sum += d;
        }
        if ((*n_it)->type() == T_WAS_HEAD) { st = "Был головным"; }

        QString state = ((*n_it)->dead()) ? "Активный" : "Не активный";

        nodes_body += QString("%1 | %2 | %3 | %4 | %5 | %6 | %7")
                .arg(QString::number((*n_it)->n()), 4)
                .arg(QString::number((*n_it)->x(), 'f', 6))
                .arg(QString::number((*n_it)->y(), 'f', 6))
                .arg(d_str)
                .arg(st)
                .arg(state)
                .arg(e_str)  + "\n";
    }

    // выплевываем энергию в гуи
    _ui->te_energy_list->append(QString("%1 | %2 | %3 | %4")
                                .arg(QString::number(_round))
                                .arg(QString::number(e_sum, 'f', 6))
                                .arg(QString::number(e_sum / _n_head, 'f', 6))
                                .arg(QString::number(d_sum / _n_head, 'f', 6))
                                );
    _ui->te_nodes_list->append(nodes_body);
}
