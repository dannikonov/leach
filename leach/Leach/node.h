#ifndef NODE_H
#define NODE_H
#include <QDebug>
enum node_type {
    T_NODE = 0, T_HEAD = 1, T_WAS_HEAD = -1
};
class Node
{
public:
    Node(double x, double y, double z, double e, int n);
    ~Node();

    int n() const;

    double x() const;
    double y() const;
    double z() const;
    double e() const;
    node_type type() const;
    bool dead() const;

    void x(double x);
    void y(double y);
    void z(double z);
    void e(double e);
    void type(node_type type);
    void dead(bool dead);

    void move(Node *target);

private:
    double _x, _y, _z, _e;
    int _n;
    node_type _type;
    bool _dead;
};


#endif // NODE_H
