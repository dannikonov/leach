#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui_(new Ui::Dialog)
{


    ui_->setupUi(this);
    /* demo */
    d_ = 2;
    ui_->dimensionLE->setText(QString::number(d_));
    ui_->dimensionLE->setEnabled(false);
    ui_->xminLE->setText("0");
    ui_->xmaxLE->setText("30");
    ui_->yminLE->setText("0");
    ui_->ymaxLE->setText("30");

    ui_->pointsLE->setText("100");
    ui_->clusterLE->setText("4");
    ui_->epsilonLE->setText("0.4");
    ui_->fuzzinessLE->setText("2");
    /* /demo */
}

Dialog::~Dialog() {
    if (data_point_) {
        for (int i = 0; i < n_; ++i) {
            delete [] data_point_[i];
        }
        delete [] data_point_;
    }

    delete ui_;
}

void Dialog::on_generatePB_clicked() {
    n_ = ui_->pointsLE->text().toInt();
    c_= ui_->clusterLE->text().toInt();
    d_= ui_->dimensionLE->text().toInt();
    m_ = ui_->fuzzinessLE->text().toDouble();
    epsilon_ = ui_->epsilonLE->text().toDouble();

    l_[0] = ui_->xminLE->text().toInt();
    l_[1] = ui_->yminLE->text().toInt();
    h_[0] = ui_->xmaxLE->text().toInt();
    h_[1] = ui_->ymaxLE->text().toInt();

    data_point_ = new double * [n_];

    for (int i = 0; i < n_; ++i) {
        data_point_[i] = new double[d_];
        for (int j = 0; j < d_; ++j) {
            data_point_[i][j] = (rand() % (h_[j] - l_[j] + 1)) + l_[j];

//            if (data_point_[i][j] < ranges_[j].first)
//                ranges_[j].first = data_point_[i][j];
//            if (data_point_[i][j] > ranges_[j].second)
//                ranges_[j].second = data_point_[i][j];
        }
    }
}

void Dialog::on_viewPointsPB_clicked() {
    QDialog *popup = new QDialog(this);
    QTextEdit *te = new QTextEdit(popup);

    for (int i = 0; i < n_; ++i) {
        QString tmp = QString("[%1]: ").arg(i);

        for (int j = 0; j < d_; ++j) {
            tmp+= QString::number(data_point_[i][j]) + " ";
        }

        te->append(tmp);
        tmp="";
    }

    popup->resize(300, 200);
    te->resize(300, 200);
    popup->show();
}

void Dialog::on_runRB_clicked() {

}

void Dialog::on_viewResultsPB_clicked() {

}
