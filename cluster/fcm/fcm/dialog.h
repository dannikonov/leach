#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QDebug>
#include <QTextEdit>
#include <QGraphicsScene>
#include <QTimer>
#include <cmath>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_generatePB_clicked();
    void on_viewPointsPB_clicked();
    void on_runRB_clicked();
    void on_viewResultsPB_clicked();

private:
    Ui::Dialog *ui_;
    QGraphicsScene *scene_;

    int n_; // points
    int c_; // clusters
    int d_; // dimensions (2)
    double m_; // fuzziness >= 1
    int l_[2], h_[2]; // lowest and highest values
    double epsilon_; // 0 < e < 1

    QVector< QBrush > brush_;
    QVector< QPair<double, double> > ranges_;
    double **data_point_;
    double **degree_of_memb_;
    double **cluster_centre_;

    void updateScene();
    void drawGraph();

    void calculate_centre_vectors();
    double get_norm(int i, int j);
    double get_new_value(int i, int j);
    double update_degree_of_membership();
};

#endif // DIALOG_H
