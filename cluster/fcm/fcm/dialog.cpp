#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui_(new Ui::Dialog), scene_(new QGraphicsScene(parent))
{
    brush_.append(QBrush(Qt::red));
    brush_.append(QBrush(Qt::green));
    brush_.append(QBrush(Qt::blue));
    brush_.append(QBrush(Qt::yellow));
    brush_.append(QBrush(Qt::cyan));
    brush_.append(QBrush(Qt::magenta));
    brush_.append(QBrush(Qt::black));
    brush_.append(QBrush(Qt::gray));
//    for (int i = 0; i < pens_.size(); ++i) pens_[i].setWidth(1);

    ui_->setupUi(this);
    scene_ = new QGraphicsScene(-10, -10,
                                ui_->graphicsView->width() - 20,
                                ui_->graphicsView->height() -20,
                                ui_->graphicsView);
    qDebug() << scene_->width();
    ui_->graphicsView->setScene(scene_);

    QPen black = QPen(Qt::black);
    black.setWidth(1);
    scene_->addLine(0,0,scene_->width(), scene_->height(),black);
    ui_->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui_->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    /* demo */
    d_ = 2;
    ui_->dimensionLE->setText(QString::number(d_));
    ui_->dimensionLE->setEnabled(false);
    ui_->xminLE->setText("0");
    ui_->xmaxLE->setText("50");
    ui_->yminLE->setText("0");
    ui_->ymaxLE->setText("50");

    ui_->pointsLE->setText("100");
    ui_->clusterLE->setText("4");
    ui_->epsilonLE->setText("0.4");
    ui_->fuzzinessLE->setText("2");

    ui_->runRB->setEnabled(false);
    /* /demo */
}

Dialog::~Dialog() {
    if (data_point_) {
        for (int i = 0; i < n_; ++i) {
            delete [] data_point_[i];
        }
        delete [] data_point_;
    }

    if (degree_of_memb_) {
        for (int i = 0; i < n_; ++i) {
            delete []degree_of_memb_[i];
        }
        delete [] degree_of_memb_;
    }

    if (cluster_centre_) {
        for (int i = 0; i < c_; ++i) {
            delete [] cluster_centre_[i];
        }
        delete [] cluster_centre_;
    }


    delete ui_;
}

void Dialog::on_generatePB_clicked() {
    ui_->runRB->setEnabled(true);
    ui_->runRB->setText("Run");

    n_ = ui_->pointsLE->text().toInt();
    c_= ui_->clusterLE->text().toInt();
    d_= ui_->dimensionLE->text().toInt();
    m_ = ui_->fuzzinessLE->text().toDouble();
    epsilon_ = ui_->epsilonLE->text().toDouble();

    l_[0] = ui_->xminLE->text().toInt();
    l_[1] = ui_->yminLE->text().toInt();
    h_[0] = ui_->xmaxLE->text().toInt();
    h_[1] = ui_->ymaxLE->text().toInt();

    // init data_point_
    data_point_ = new double * [n_];
    for (int j = 0; j < d_; ++j) {
        ranges_.insert(j, QPair<double, double>(l_[j], h_[j]));
    }

    for (int i = 0; i < n_; ++i) {
        data_point_[i] = new double[d_];
        for (int j = 0; j < d_; ++j) {
            data_point_[i][j] = (rand() % (h_[j] - l_[j] + 1)) + l_[j];

            if (data_point_[i][j] < ranges_[j].first)
                ranges_[j].first = data_point_[i][j];
            if (data_point_[i][j] > ranges_[j].second)
                ranges_[j].second = data_point_[i][j];

        }
    }

    // init degree_of_memb_
    double s;
    int r, rval;
    degree_of_memb_ = new double * [n_];

    for (int i = 0; i < n_; ++i) {
        s = 0.0;
        r = 100;
        degree_of_memb_[i] = new double[c_];
        for (int j = 1; j < c_; ++j) {
            rval = rand() % (r + 1);
            r -= rval;
            degree_of_memb_[i][j] = rval / 100.0;
            s += degree_of_memb_[i][j];
        }
        degree_of_memb_[i][0] = 1.0 - s;
    }

    // init cluster_centre_
    cluster_centre_ = new double * [c_];
    for (int i = 0; i < c_; ++i) {
        cluster_centre_[i] = new double[d_];
    }

    updateScene();

}

void Dialog::on_viewPointsPB_clicked() {
    QDialog *popup = new QDialog(this);
    QTextEdit *te = new QTextEdit(popup);

    for (int i = 0; i < n_; ++i) {
        QString tmp = QString("[%1]: ").arg(i);

        for (int j = 0; j < d_; ++j) {
            tmp+= QString::number(data_point_[i][j]) + " ";
        }

        te->append(tmp);
        tmp="";
    }

    te->resize(300, 200);
    popup->resize(300, 200);
    popup->show();
}

void Dialog::on_runRB_clicked() {


    ui_->runRB->setText("running");
    double max_diff;
    do {
        calculate_centre_vectors();
        max_diff = update_degree_of_membership();

    } while (max_diff > epsilon_);
    updateScene();
}

void Dialog::on_viewResultsPB_clicked() {
    QDialog *popup = new QDialog(this);
    QTextEdit *te = new QTextEdit(popup);

    for (int i = 0; i < n_; ++i) {
        QString tmp = QString("[%1]: ").arg(i);

        for (int j = 0; j < c_; ++j) {
            tmp+= QString::number(degree_of_memb_[i][j]) + "  ";
        }

        te->append(tmp);
        tmp="";
    }

    te->resize(300, 200);
    popup->resize(300, 200);
    popup->show();
}

void Dialog::updateScene() {
    drawGraph();
}

void Dialog::drawGraph() {
    scene_->clear();
    QPen black = QPen(Qt::black);
    black.setWidth(1);

    double x_koef = scene_->width() / ranges_[0].second;
    double y_koef = scene_->height() / ranges_[1].second;


    int cluster;
    double highest;

    for (int i = 0; i < n_; ++i) {
        cluster = 0;
        highest = 0.0;
        for (int j = 0; j < c_; ++j) {
            if (degree_of_memb_[i][j] > highest) {
                highest = degree_of_memb_[i][j];
                cluster = j;
            }
        }
        scene_->addEllipse(data_point_[i][0] * x_koef, data_point_[i][1] * y_koef, 6, 6, QPen(), brush_[cluster]);
    }
}

void Dialog::calculate_centre_vectors() {
    double numerator, denominator;
    double t[n_][c_];

    for (int i = 0; i < n_; ++i) {
        for (int j = 0; j < c_; ++j) {
            t[i][j] = pow(degree_of_memb_[i][j], m_);
        }
    }

     for (int i = 0; i < c_; ++i) {
         for (int j = 0; j < d_; ++j) {
             numerator = 0.0;
             denominator = 0.0;
             for (int k = 0; k < n_; ++k) {
                 numerator += t[k][i] * data_point_[k][j];
                 denominator += t[k][i];
             }
             cluster_centre_[i][j] = numerator / denominator;
         }
     }
}

double Dialog::get_norm(int i, int j) {
    int k;
    double sum = 0.0;
    for (k = 0; k < d_; ++k) {
        sum += pow(data_point_[i][k] - cluster_centre_[j][k], 2);
    }
    return sqrt(sum);
}

double Dialog::get_new_value(int i, int j) {
    double t, p, sum;
    sum = 0.0;
    p = 2 / (m_ - 1);
    for (int k = 0; k < c_; ++k) {
        t = get_norm(i, j) / get_norm(i, k);
        t = pow(t, p);
        sum += t;
    }
    return 1.0 / sum;
}

double Dialog::update_degree_of_membership() {
    double new_uij;
    double max_diff = 0.0, diff;
    for (int j = 0; j < c_; ++j) {
        for (int i = 0; i < n_; ++i) {
            new_uij = get_new_value(i, j);
            diff = new_uij - degree_of_memb_[i][j];
            if (diff > max_diff)
                max_diff = diff;
            degree_of_memb_[i][j] = new_uij;
        }
    }
    return max_diff;
}
